<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_reference_ajax_formatter\FunctionalJavascript;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/**
 * Test the Entity Reference Ajax Formatter.
 *
 * @group entity_reference_ajax_formatter
 */
class EntityReferenceAjaxFormatterTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The Entity View Display for the article node type.
   *
   * @var \Drupal\Core\Entity\Entity\EntityViewDisplay
   */
  protected EntityViewDisplayInterface $display;

  /**
   * The primary node to be testing.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected NodeInterface $node;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'entity_reference_ajax_formatter',
  ];

  /**
   * Tests the behavior of the 'entity_reference_ajax_entity_view' formatter.
   */
  public function testFormatter(): void {
    $this->drupalGet("node/{$this->node->id()}");
    $session = $this->assertSession();
    $session->pageTextContains('Node #1');
    $session->pageTextNotContains('Node #2');
    $session->pageTextNotContains('Load More');

    // Test random sort.
    $this->display->setComponent('field_ref', [
      'type' => 'entity_reference_ajax_entity_view',
      'settings' => [
        'number' => 3,
        'sort' => 1,
        'load_more' => TRUE,
        'max' => 8,
      ],
    ])->save();

    $this->drupalGet("node/{$this->node->id()}");
    $page = $this->getSession()->getPage();

    // Selector is a bit gross with stark's lack of classes/so many divs.
    $this->assertSame(count($page->findAll('css', 'article')), 8);
    $session->pageTextMatches('/Node #(\d\d|[^123])/');
    $session->pageTextContains('Load More');
    $page->clickLink('Load More');
    $session->assertExpectedAjaxRequest(1);
    $this->assertSame(count($page->findAll('css', 'article')), 14);
    $session->pageTextContains('Load More');

    $page->clickLink('Load More');
    $session->assertExpectedAjaxRequest(2);
    $this->assertSame(count($page->findAll('css', 'article')), 18);
    $session->pageTextNotContains('Load More');
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalCreateContentType(['type' => 'article']);
    $user = $this->drupalCreateUser([
      'create article content',
      'edit own article content',
    ]);
    $this->drupalLogin($user);
    $entityTypeManager = $this->container->get('entity_type.manager');
    FieldStorageConfig::create([
      'field_name' => 'field_ref',
      'entity_type' => 'node',
      'type' => 'entity_reference',
      'cardinality' => '-1',
      'settings' => [
        'target_type' => 'node',
      ],
    ])->save();
    FieldConfig::create([
      'field_name' => 'field_ref',
      'label' => 'Node references',
      'entity_type' => 'node',
      'bundle' => 'article',
      'settings' => [
        'handler' => 'default:node',
        'handler_settings' => [
          'target_bundles' => [
            'article' => 'article',
          ],
          'sort' => [
            'field' => '_none',
          ],
        ],
      ],
    ])->save();
    $this->display = $entityTypeManager->getStorage('entity_view_display')
      ->load('node.article.default');
    $this->display->setComponent('field_ref', [
      'type' => 'entity_reference_ajax_entity_view',
      'settings' => [
        'number' => 2,
      ],
    ])->save();
    $nodes = [];
    $i = 0;
    while ($i <= 9) {
      $node = Node::create([
        'title' => "Node #{$i}",
        'type' => 'article',
      ]);
      $node->save();
      $nodes[] = ['target_id' => $node->id()];
      $i++;
    }
    $this->node = Node::create([
      'title' => 'Primary Node',
      'type' => 'article',
      'field_ref' => $nodes,
    ]);
    $this->node->save();
  }

}
